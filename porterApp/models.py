# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
from django.db import models
from django.core.urlresolvers import reverse

class ShipmentModel(models.Model):
	fromAddr = models.CharField(max_length=200)
	toAddr   = models.CharField(max_length=200)
	name     = models.CharField(max_length=200)
